# Protocol Specification

### Summary

------------

HAZAMA is a bitcoin side chain technology built using the federated peg model. It allows the 2 way peg of BTC, Counterparty and other blockchain assets such as ERC-20 to a semi-centralized yet cheaper and faster side chain for use with low value assets requiring a high user experience such as gaming and app tokens. With HAZAMA technology industry and communities are free to customize the properties of their chain to match their own regulatory or technical requirements. Such as low or high fees, whitelisted/blacklisted assets etc.

### How it works

------------

HAZAMAs security is based around the federated peg model where a user deposits an asset they wish to peg in to multi sig address controlled by a group of trusted entities. Once the trusted entities have received the asset they issue an analogous token on the side chain. Once a token is "Pegged in" it can be sent and traded on the side chain using the built in wallet and Decentralized Exchange with low fees and fast block times. They can also link their wallet to compatible games and apps. At any point a user can "Peg out" their asset to receive the initial asset on the parent chain.
see [whitepaper](https://docs.wixstatic.com/ugd/b177dd_dd2885c0871140ff840e415b1b1cc163.pdf).

### Peg In/Out

------------

Pegging public blockchain assets to HAZAMA is very easy. Just send it to a dedicated peg-in address. This is uniquely determined for each blockchain.
For example, if it is a bitcoin blockchain (testnet), it is ``mr93L6DSfHEbbRA1Vx8vSAuaZzKxNkSVmo``.

To peg out, send HAZAMA assets to a unique dedicated address. ``hazamaxxxxxxxxxxxxxxxxxxxxxxzeWuNC``.

**Note:** There are NO peg In/Out addresses other than those listed here. Please do NOT to send it to another address.

### Transactions

------------

HAZAMA messages have the following components:

- Source addresses
- Destination addresses (optional)
- A quantity of bitcoins sent from the sources to the destinations, if it exists.
- A fee, in bitcoins, paid to the Bitcoin miners who include the transaction in a block.
- Some ‘data’, imbedded in specially constructed transaction outputs.

Every blockchain transaction carrying a HAZAMA transaction has the following possible outputs:

- zero or more destination outputs,
- zero or more data outputs, and optional change outputs.

All data outputs follow all destination outputs. Change outputs (outputs after the last data output) have no significance.

For identification purposes, every HAZAMA transaction’s ‘data’ field is prefixed by the string ``CNTRPRTY``, encoded in UTF‐8. This string is long enough that transactions with outputs containing pseudo‐random data cannot be mistaken for valid HAZAMA transactions. In testing (i.e. using the TESTCOIN HAZAMA network on any blockchain), this string is ‘XX’.

HAZAMA data may be stored in three different types of outputs, or in some combinations of those formats. All of the data is obfuscated by ARC4 encryption using the transaction identifier (TXID) of the first unspent transaction output (UTXO) as the encryption key.

Multi‐signature data outputs are one‐of‐three outputs where the first public key is that of the sender, so that the value of the output is redeemable, and the second two public keys encode the data, zero‐padded and prefixed with a length byte.

The data may also be stored in ``OP_RETURN`` outputs or as fake pubkeyhashes.

The existence of the destination outputs, and the significance of the size of the fee, depend on the HAZAMA message type, which is determined by the four bytes in the data field that immediately follow the identification prefix. The rest of the data have a formatting specific to the message type, described in the source code.

The sources and destinations of a HAZAMA transaction are HAZAMA addresses, and may be either ``OP_CHECKSIG`` and ``OP_CHECKMULTISIG`` Bitcoin ScriptPubkeys.

All messages are parsed in order, one at a time, ignoring block boundaries.

Orders, order matches matches are expired at the end of blocks.

### mempool transactions

--------------------

Always have block index = 9999999 (``config.MEMPOOL_BLOCK_INDEX``).

DB changes never persist across sessions.

### Assets

------

All assets have the following properties:

-  Asset name
-  Asset ID
-  Description
-  Divisiblity

Newly registered asset names will be either (unique) strings of 4 to 12 uppercase Latin characters (inclusive) not beginning with ‘A’, or integers between 26^12 + 1 and 256^8 (inclusive), prefixed with ‘A’. Example asset names: BBBB, A100000000000000000.

Assets may be either divisible or indivisible, and divisible assets are divisible to eight decimal places. Assets also come with descriptions, which may be up to 52 single-byte characters long and updated at any time.

### Subassets

------

1. Subasset names must meet following requirements :
    *   Begin with the parent asset name followed by a period (.)
    *   Contain at least 1 character following the parent asset name and a period (.) (e.g. PIZZA.x)
    *   Contain up to 250 characters in length including the parent asset name (e.g. PIZZA.REALLY-long-VALID-Subasset-NAME)
    *   Contain only characters `a-zA-Z0-9.-_@!`
    *   Cannot end with a period (.)
    *   Cannot contain multiple consecutive periods (..)

2. A subasset may only be issued from the same address that owns the parent asset at the time of the issuance
3. A subasset may be transferred to a new owner address after initial issuance
4. A subasset has an anti-spam issuance cost of 0.25 XCP

### Memos

------

A Memo can be attached to a send transactions. When a shared public address is used for incoming transactions, a memo may be used to link an incoming payments with a specific user account identifier or invoice.  Memos do not need to be unique.  Multiple sends may have the same memo.

The Memo is a numeric value expressed in hexadecimal or a UTF-8 encoded text string. Valid memos are no more than 34 bytes long.


### Transaction Statuses

--------------------

*Offers* (i.e. orders) are given a status ``filled`` when their ``give_remaining``, ``get_remaining``, ``fee_provided_remaining`` or ``fee_required_remaining`` are no longer positive quantities.

Because order matches pending BTC payment may be expired, orders involving Bitcoin cannot be filled, but remain always with a status ``open``.

### Message Types

-------------

-  Send
-  Order
-  Issue
-  Broadcast
-  Dividend
-  Cancel
-  Destroy

#### Send

A **send** message sends a quantity of any HAZAMA asset from the source address to the destination address. If the sender does not hold a sufficient quantity of that asset at the time that the send message is parsed (in the sequence of transactions), then the send is considered an oversend.

Oversends are handled as follows:

1) Oversends using the legacy send transaction type are valid and filled as much as they can be
2) Oversends using the new default enhanced send transaction type are invalid and none of the asset is sent

#### Order

An ‘order’ is an offer to *give* a particular quantity of a particular asset and *get* some quantity of some other asset in return. No distinction is drawn between a ‘buy order’ and a ‘sell order’. The assets being given are escrowed away immediately upon the order being parsed. That is, if someone wants to give 1 XCP for 2 BTC, then as soon as he publishes that order, his balance of XCP is reduced by one.

When an order is seen in the blockchain, the protocol attempts to match it, deterministically, with another open order previously seen. Two matched orders are called a ‘order match’. the trade is completed immediately, with the protocol itself assigning the participating addresses their new balances.

All orders are *limit orders*: an asking price is specified in the ratio of how much of one would like to get and give; an order is matched to the open order with the best price below the limit, and the order match is made at *that* price. That is, if there is one open order to sell at .11 XCP/ASST, another at .12 XCP/ASST, and another at .145 XCP/BTC, then a new order to buy at .14 XCP/ASST will be matched to the first sell order first, and the XCP and BTC will be traded at a price of .11 XCP/ASST, and then if any are left, they’ll be sold at .12 XCP/ASST. If two existing orders have the same price, then the one made earlier will match first.

All orders allow for partial execution; there are no all‐or‐none orders. If, in the previous example, the party buying the bitcoins wanted to buy more than the first sell offer had available, then the rest of the buy order would be filled by the latter existing order. After all possible order matches are made, the current (buy) order is listed as an open order itself. If there exist multiple open orders at the same price, then order that was placed earlier is matched first.

Open orders expire after they have been open for a user‐specified number of blocks. When an order expires, all escrowed funds are returned to the parties that originally had them.

In general, there can be no such thing as a fake order, because the assets that each party is offering are stored in escrow. Partial orders pay partial fees. These fees are designated in the code as ``fee_required`` and ``fee_provided``, and as orders involving BTC are matched (expired), these fees (required and provided) are debited (sometimes replenished), in proportion to the fraction of the order that is matched. That is, if an order to sell 1 BTC has a ``fee_provided`` of 0.01 BTC (a 1%), and that order matches for 0.5 BTC initially, then the ``fee_provided_remaining`` for that order will thenceforth be 0.005 BTC. *Provided* fees, however, are not replenished upon failure to make BTC payments, or their anti‐trolling effect would be voided.

#### Issue

Assets are issued with the **issuance** message type: the user picks a name and a quantity, and the protocol credits his address accordingly. The asset name must either be unique or be one previously issued by the same address. When re‐issuing an asset, that is, issuing more of an already‐issued asset, the divisibilities and the issuing address must match.

The rights to issue assets under a given name may be transferred to any other address.

Assets may be locked irreversibly against the issuance of further quantities and guaranteeing its holders against its inflation. To lock an asset, set the description to ‘LOCK’ (case‐insensitive).

Issuances of any non‐zero quantity, that is, issuances which do not merely change, e.g., the description of the asset, involve a debit (and destruction) of now 0.5 XCP.

Asset descriptions in enhanced asset information schema may be of arbitrary length.

#### Broadcast

A **broadcast** message publishes textual and numerical information, along with a timestamp, as part of a series of broadcasts called a ‘feed’. One feed is associated with one address: any broadcast from a given address is part of that address’s feed. The timestamps of a feed must increase monotonically.

The text field may be of arbitrary length.

A feed is identified by the address which publishes it.

#### Dividend

A dividend payment is a payment of some quantity of any HAZAMA asset to every holder of a an asset (except BTC or XCP or other native assets) in proportion to the size of their holdings. Dividend‐yielding assets may be either divisible or indivisible. A dividend payment to any asset may originate from any address. The asset for dividend payments and the assets whose holders receive the payments may be the same.

-  TODO: dividends on escrowed funds

There is a small anti-spam fee of 0.0002 XCP per recipient with dividends.

#### Cancel

Open offers may be cancelled, which cancellation is irrevocable.

A *cancel* message contains only the hash of the Bitcoin transaction that contains the order to be cancelled. Only the address which made an offer may cancel it.

#### Destroy

A **destroy** message sends a quantity of any HAZAMA asset from the source address to the default burn address. If the sender does not hold a sufficient quantity of that asset at the time that the destroy message is parsed (in the sequence of transactions), then the destroy is considered invalid.