# Messages Specification

The following communication protocols allow a wallet to interact with the hazama environment in a streamlined way.


## Gateway On-chain

On-chain messages from/to the hazana gateways occurs as counterparty encoded messages into the private bitcoin chain.

The following messages are the predicates supported by the hazama environment to process the users' actions:

  * Broadcast: `NewDepAddr(chain)`, Source: User
    - Asks generation of a main chain token deposit address. It allows depositing BTC or ETH into hazama.
  * Broadcast: `A(tx_index, address)`, Source: Gateway
    - Reports main chain deposit address corresponding to a previous `NewDepAddr` referred by `tx_index`.
  * Send:
    - Destination: Gateway address
    - Memo: Withdraw address (on main chain)
    - Asks withdrawal of assets into main chain. Can be used for native coins (BTC or ETH), XCP assets and ERC20 assets.
  * Memo send: `PAY(tx_index)`, Source: User
    - Marks this send as a payment for a withdraw referred by `tx_index`.

## Gateway IRC

Certain out of band information exchange between the user wallet and the gateways can occur before commiting resources into the private chain.
These messages allow the wallet to decide and show the user what kind of operations they can do before sending them into the private chain.

  * `lst` (from user)
    - Returns a list of possible assets along how much of them is needed to pay for any withdraw for each gateway. Also shows the withdraw address into the private chain for each gateway.
  * `inv` (from user)
    - Queries the gateways about the available withdraw inventory, so the user's wallet can determine what's available to withdraw from each gateway.

## VM On-chain

External LUA VMs can be created inside hazama to react to certain events or allow the users to call certain functions on tokens.

  * Broadcast: `vm_prime \n pubkey`, Source: User
    - Primes a VM P2SH address with the user's pubkey. The resulting address can be calculated using the VM processor's well known pubkey.
  * Broadcast: `vm_create \n code`, Source: VM P2SH address
    - Creates/Replaces a VM at the given P2SH address.
  * Broadcast: `vm_call \n vm_address \n method \n parameters`, Source: User
    - Calls a VM with a given set of parameters.
