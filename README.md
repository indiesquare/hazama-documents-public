# HAZAMA Docs

---

[Protocol Specification](./protocol_specification.md)

[Assets Depth](./hazama-assets-depth.md)

[HAZAMA Server API](./hazama-server-api.md)